# Getting Started with assignemtn App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## To run project

1.npm install(root folder) - to install server side depandancies

2.cd clients/assignment-frontend

3.npm install(react install) - to install client side depandancies

4. cd ../..

5. npm start (root folder)

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
will automatically run node server and react app simultaneously 

### `npm test`

Launches the test runner in the interactive watch mode.\

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.
