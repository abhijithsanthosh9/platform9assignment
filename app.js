const express = require('express'),
app = express()

require('dotenv').config()


// app.get('/', (req, res) => {
//     res.send('hello')
// })

const PORT = process.env.PORT || 3001

var bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())


app.use('/api/', require('./routes/hello'))
app.use('/api/tweets', require('./routes/tweets'))
app.use('/api/retweets', require('./routes/retweet'))


app.listen(PORT, ()=> {
    console.log(`Listening on Port: ${PORT}`)
})