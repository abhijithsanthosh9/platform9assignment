import React, {useEffect, useState} from 'react';
import PaginationList from 'react-pagination-list';
import Twitter from 'twitter';
import cx from "classnames";
import moment from 'moment'
import ReactLoading from 'react-loading';
import styles from '../pages/landingPage/Twitter.module.css'

export default function LandingPageComponent() {
    const [data, setData] = useState([])
    const [userName, setUsername] = useState('Platform9Sys')
    const [dataresponse, setDataResponse] = useState()
    const [loading, setLoading] = useState(true)

    useEffect(() => {
         searchUser();
    },[])

    const searchUser = () => {
        setLoading(true)
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username: userName })
        }
        fetch('/api/tweets', requestOptions).then(res => {
            if(res.ok) {
                return res.json()
            }
        }).then((jsonResponse) => {
            setDataResponse(jsonResponse)
            console.log(jsonResponse)
            var remainingDataArray = [];
            var mediaContentArray = [];
            var tempName;
            jsonResponse.tweets.data.map((data)=> {
                if(data.attachments != undefined) {
                    if(jsonResponse.tweets.includes != undefined) {
                        jsonResponse.tweets.includes.media.map((testData) => {
                            // console.log(testData.media_key)
                            if(testData.media_key == data.attachments.media_keys[0]) {
                                console.log(testData.url)
                                tempName = testData.url;
                            }
                        })
                    }
                } else {
                        tempName = null
                }
                var totalContent = {
                    "user": jsonResponse.userProfile.data[0].name,
                    "userName":jsonResponse.userProfile.data[0].username,
                    "text":data.text,
                    "url": tempName,
                    "profileImg": jsonResponse.userProfile.data[0].profile_image_url
                }
                remainingDataArray.push(totalContent)
                })
                console.log(remainingDataArray)
                setData(remainingDataArray)
            setLoading(false)
        })
    }

    return( 
            <div data-testid="landing-1"  className="h-full">
                <header className="bg-blue-600 w-full h-16" />
                <div className="md:flex lg:flux h-full">
                    <div className="border-r-2 md:h-full lg:h-full w-3/12 ">
                        <div className={cx(styles.searchContainer)}>
                            
                            <label className={cx(styles.searchlabel,"text-grey-700 text-sm pb-2")}>Enter Twiter ID</label>
                                <input type="text" placeholder="Enter twiterId" className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none focus:ring w-full"
                                onChange={(e)=> setUsername(e.target.value)}
                                defaultValue={userName}/>
                                <button className="text-blue-600 bg-transparent border border-solid border-blue-600 hover:bg-blue-600 hover:text-white active:bg-blue-600 font-bold uppercase text-sm px-6 py-3 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 mt-3 w-full" type="submit" onClick={(e) => searchUser(e)
                                }
                                >
                                    Search
                                </button>
                            
                            </div>
                    </div>
                    <div className={cx(styles.tweetContainer,"w-4/12 border-r-2 h-full")}>
                        <div className="border-b-2 shadow-xl p-4    ">
                            Tweets
                        </div>
                        {loading && <div className={cx(styles.loading)}>
                            <ReactLoading type='balls' color='blue' height={66} width={37} />
                            </div>}
                        {!loading && <PaginationList 
                            data={data}
                            pageSize={4}
                            renderItem={(item, key) => (
                                <Cards item={item}/>
                            )}
                        ></PaginationList>}
                    </div>
                    <div className={cx(styles.profileContainer)}>
                        {!loading &&<div className="justify-center w-full mt-3 pl-3">
                                <div>
                                    <img className="inline-block h-20 w-20 rounded-full" src={dataresponse.userProfile.data[0].profile_image_url} alt="" />
                                </div>
                                <div className={cx(styles.userName)}>
                                    <h2 className="text-xl leading-6 font-bold text-black">{dataresponse.userProfile.data[0].name}</h2>
                                    <p className="text-sm leading-5 font-medium text-gray-600">{dataresponse.userProfile.data[0].username}</p>
                                </div>
                                <div className={cx(styles.userName2,"mt-3")}>
                                    <p className="text-black leading-tight mb-2">{dataresponse.userProfile.data[0].description}</p>
                                    <div className="text-gray-600">
                                        <div className="flex mr-2"><svg viewBox="0 0 24 24" class="h-5 w-5 paint-icon"><g><path d="M19.708 2H4.292C3.028 2 2 3.028 2 4.292v15.416C2 20.972 3.028 22 4.292 22h15.416C20.972 22 22 20.972 22 19.708V4.292C22 3.028 20.972 2 19.708 2zm.792 17.708c0 .437-.355.792-.792.792H4.292c-.437 0-.792-.355-.792-.792V6.418c0-.437.354-.79.79-.792h15.42c.436 0 .79.355.79.79V19.71z"></path><circle cx="7.032" cy="8.75" r="1.285"></circle><circle cx="7.032" cy="13.156" r="1.285"></circle><circle cx="16.968" cy="8.75" r="1.285"></circle><circle cx="16.968" cy="13.156" r="1.285"></circle><circle cx="12" cy="8.75" r="1.285"></circle><circle cx="12" cy="13.156" r="1.285"></circle><circle cx="7.032" cy="17.486" r="1.285"></circle><circle cx="12" cy="17.486" r="1.285"></circle></g></svg> <span class="leading-5 ml-1">Joined {moment(dataresponse.userProfile.data[0].created_at).format("MMMM YYYY")}</span></div>
                                    </div>
                                </div>
                                <div className="pt-3 flex justify-start items-start w-full divide-x divide-gray-800 divide-solid">
                                    <div className="text-center pr-3"><span className="font-bold text-black">{dataresponse.userProfile.data[0].public_metrics.following_count}</span><span className="text-gray-600"> Following</span></div>
                                    <div className="text-center px-3"><span className="font-bold text-black">{dataresponse.userProfile.data[0].public_metrics.followers_count}</span><span className="text-gray-600"> Followers</span></div>
                                </div>
                            </div>
                        }
                        </div>
                        <hr class="border-gray-800"/>
                </div>
            </div>
    )
}

const Cards = ({item}) => {

    const retweets = () => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username: 'test' })
        }
        fetch('/api/retweets', requestOptions).then(res => {
            if(res.ok) {
                return res.json()
            }
        }).then((jsonResponse) => {
            console.log(jsonResponse)
        })
    }
    
    return (
        <div className={cx(styles.cardData,"ml-20 mr-20 border shadow-lg")}>
            <div className="flex p-4 pb-0 ml-4 mr-4 ">
                <div className="flex items-center">
                    <div>
                      <img className="inline-block h-10 w-10 rounded-full" src={item.profileImg} alt="" />
                    </div>
                    <div className="ml-3">
                        <p className="text-base leading-6 font-medium text-black">
                            {item.user}
                            <span className={cx(styles.userID,"text-sm leading-5 font-medium text-gray-400 group-hover:text-gray-300 transition ease-in-out duration-150")}>
                                @{item.userName}
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div className={cx(styles.image)}>
                <img className="w-20" src={item.url} alt="" />
            </div>
            <div className={cx(styles.description, "p-10")}>
                <p className="text-base width-auto font-medium text-black pl-4">
                    {item.text}
                  {/* <span className="text-blue-400"> #WomenWhoCode #CodeNewbie</span> */}
                </p>
                {/* */}
            </div>
            {/* <div onClick={()=>retweets()}>
                retweet
            </div> */}
        </div>
    )
    }