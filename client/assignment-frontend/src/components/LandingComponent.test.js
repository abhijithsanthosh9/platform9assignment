import { render , screen , cleanup } from '@testing-library/react'
import LandingComponent from './LandingComponent'

test('should render todo componenent' , () => {
    render(<LandingComponent />);
    const landingElement = screen.getByTestId('landing-1');
    expect(landingElement).toBeInTheDocument();
})