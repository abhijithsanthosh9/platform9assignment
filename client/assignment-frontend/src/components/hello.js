import React, {useEffect, useState} from 'react';

export const Hello = () => {
    const [initialState, setInitialState] = useState([]);

    const tweetUser = (e) => {
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ username: e.target.value })
        }
        fetch('/api/tweets', requestOptions).then(res => {
            if(res.ok) {
                return res.json()
            }
        }).then(jsonResponse => setInitialState(jsonResponse.tweets))
    }

    console.log(initialState)
    return(
        <>
            <div className="h-full">
                <header className="bg-blue-600 w-full h-16" />
                <div className="flex h-full">
                    <div className="border-r-2 h-full w-3/12 ">
                        <div className="mb-3 pt-0 m-3">
                            {/* <form onSubmit={(e) => createRule(e)}> */}
                            {/* <label className="text-grey-700 text-sm pb-2">Enter Twiter ID</label> */}
                                <input type="text" placeholder="Enter twiterId" className="px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm border border-blueGray-300 outline-none focus:outline-none focus:ring w-full"/>
                                <button className="text-blue-600 bg-transparent border border-solid border-blue-600 hover:bg-blue-600 hover:text-white active:bg-blue-600 font-bold uppercase text-sm px-6 py-3 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 mt-3 w-full" type="submit"
                                >
                                    Search
                                </button>
                            {/* </form> */}
                            </div>
                    </div>
                    <div className="w-6/12 border-r-2 h-full">
                        <div className="border-b-2 shadow-xl p-4    ">
                            Tweets
                        </div>
                        {/* <PaginationList 
                            data={data}
                            pageSize={4}
                            renderItem={(item, key) => (
                                <Cards />
                            )}
                        ></PaginationList> */}
                    </div>
                    <div className="w-4/12 border-r-2 h-full">
                        <div className="justify-center w-full mt-3 pl-3">
                            <div>
                                    <img className="inline-block h-20 w-20 rounded-full" src="https://pbs.twimg.com/profile_images/1121328878142853120/e-rpjoJi_bigger.png" alt="" />
                            </div>
                            <div>
                                <h2 className="text-xl leading-6 font-bold text-black">ℜ𝔦𝔠𝔞𝔯𝔡𝔬ℜ𝔦𝔟𝔢𝔦𝔯𝔬.dev</h2>
                                <p className="text-sm leading-5 font-medium text-gray-600">@Ricardo_oRibeir</p>
                            </div>
                            <div className="mt-3">
                                <p className="text-black leading-tight mb-2">Software Engineer / Designer / Entrepreneur <br />Visit my website to test a working <b>Twitter Clone.</b> </p>
                                <div className="text-gray-600">
                                    <div className="flex mr-2"><svg viewBox="0 0 24 24" class="h-5 w-5 paint-icon"><g><path d="M11.96 14.945c-.067 0-.136-.01-.203-.027-1.13-.318-2.097-.986-2.795-1.932-.832-1.125-1.176-2.508-.968-3.893s.942-2.605 2.068-3.438l3.53-2.608c2.322-1.716 5.61-1.224 7.33 1.1.83 1.127 1.175 2.51.967 3.895s-.943 2.605-2.07 3.438l-1.48 1.094c-.333.246-.804.175-1.05-.158-.246-.334-.176-.804.158-1.05l1.48-1.095c.803-.592 1.327-1.463 1.476-2.45.148-.988-.098-1.975-.69-2.778-1.225-1.656-3.572-2.01-5.23-.784l-3.53 2.608c-.802.593-1.326 1.464-1.475 2.45-.15.99.097 1.975.69 2.778.498.675 1.187 1.15 1.992 1.377.4.114.633.528.52.928-.092.33-.394.547-.722.547z"></path><path d="M7.27 22.054c-1.61 0-3.197-.735-4.225-2.125-.832-1.127-1.176-2.51-.968-3.894s.943-2.605 2.07-3.438l1.478-1.094c.334-.245.805-.175 1.05.158s.177.804-.157 1.05l-1.48 1.095c-.803.593-1.326 1.464-1.475 2.45-.148.99.097 1.975.69 2.778 1.225 1.657 3.57 2.01 5.23.785l3.528-2.608c1.658-1.225 2.01-3.57.785-5.23-.498-.674-1.187-1.15-1.992-1.376-.4-.113-.633-.527-.52-.927.112-.4.528-.63.926-.522 1.13.318 2.096.986 2.794 1.932 1.717 2.324 1.224 5.612-1.1 7.33l-3.53 2.608c-.933.693-2.023 1.026-3.105 1.026z"></path></g></svg> <a href="https://ricardoribeirodev.com/personal/" target="#" class="leading-5 ml-1 text-blue-400">www.RicardoRibeiroDEV.com</a></div>
                                    <div className="flex mr-2"><svg viewBox="0 0 24 24" class="h-5 w-5 paint-icon"><g><path d="M19.708 2H4.292C3.028 2 2 3.028 2 4.292v15.416C2 20.972 3.028 22 4.292 22h15.416C20.972 22 22 20.972 22 19.708V4.292C22 3.028 20.972 2 19.708 2zm.792 17.708c0 .437-.355.792-.792.792H4.292c-.437 0-.792-.355-.792-.792V6.418c0-.437.354-.79.79-.792h15.42c.436 0 .79.355.79.79V19.71z"></path><circle cx="7.032" cy="8.75" r="1.285"></circle><circle cx="7.032" cy="13.156" r="1.285"></circle><circle cx="16.968" cy="8.75" r="1.285"></circle><circle cx="16.968" cy="13.156" r="1.285"></circle><circle cx="12" cy="8.75" r="1.285"></circle><circle cx="12" cy="13.156" r="1.285"></circle><circle cx="7.032" cy="17.486" r="1.285"></circle><circle cx="12" cy="17.486" r="1.285"></circle></g></svg> <span class="leading-5 ml-1">Joined December, 2019</span></div>
                                </div>
                            </div>
                            <div className="pt-3 flex justify-start items-start w-full divide-x divide-gray-800 divide-solid">
                                <div className="text-center pr-3"><span className="font-bold text-black">520</span><span className="text-gray-600"> Following</span></div>
                                <div className="text-center px-3"><span className="font-bold text-black">23,4m </span><span className="text-gray-600"> Followers</span></div>
                            </div>
                        </div>
                    </div>
                    <hr class="border-gray-800"/>
                </div>
            </div>
        </>
    )
}

const Cards = () => {
    return (
        <>
            <div className="flex flex-shrink-0 p-4 pb-0">
                <div className="flex items-center">
                    <div>
                      <img className="inline-block h-10 w-10 rounded-full" src="https://pbs.twimg.com/profile_images/1121328878142853120/e-rpjoJi_bigger.png" alt="" />
                    </div>
                    <div className="ml-3">
                        <p className="text-base leading-6 font-medium text-black">
                            Sonali Hirave 
                            <span className="text-sm leading-5 font-medium text-gray-400 group-hover:text-gray-300 transition ease-in-out duration-150">
                                @ShonaDesign . 16 April
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div className="pl-16">
                <p className="text-base width-auto font-medium text-black flex-shrink">
                  Day 07 of the challenge <span className="text-blue-400">#100DaysOfCode</span> 
                  I was wondering what I can do with <span className="text-blue-400">#tailwindcss</span>, so just started building Twitter  UI using Tailwind and so far it looks so promising. I will post my code after completion.
                  [07/100]
                  <span className="text-blue-400"> #WomenWhoCode #CodeNewbie</span>
                </p>
                <div className="flex">
                    <div className="w-full">  
                        <div className="flex items-center">
                            <div className="flex-1 text-center">
                                <a href="#" className="w-12 mt-1 group flex items-center text-gray-500 px-3 py-2 text-base leading-6 font-medium rounded-full hover:bg-blue-800 hover:text-blue-300">
                                <svg className="text-center h-7 w-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke="currentColor" viewBox="0 0 24 24"><path d="M7 16V4m0 0L3 8m4-4l4 4m6 0v12m0 0l4-4m-4 4l-4-4"></path></svg>
                                </a>
                            </div>
                            <div className="flex-1 text-center py-2 m-2">
                                <a href="#" className="w-12 mt-1 group flex items-center text-gray-500 px-3 py-2 text-base leading-6 font-medium rounded-full hover:bg-blue-800 hover:text-blue-300">
                                    <svg className="text-center h-7 w-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke="currentColor" viewBox="0 0 24 24"><path d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-8l-4-4m0 0L8 8m4-4v12"></path></svg>
                            </a>
                            </div>
                            <div className="flex-1 text-center py-2 m-2">
                                <a href="#" className="w-12 mt-1 group flex items-center text-gray-500 px-3 py-2 text-base leading-6 font-medium rounded-full hover:bg-blue-800 hover:text-blue-300">
                                    <svg className="text-center h-7 w-6" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke="currentColor" viewBox="0 0 24 24"><path d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"></path></svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
  
}