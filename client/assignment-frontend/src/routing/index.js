import {
    BrowserRouter as Router,
    Switch,
    Route,
  } from "react-router-dom";
  import LandingPage from "../pages/landingPage/index"

  export default function App() {
    return (
      <Router>
          <Switch>
            <Route path="/">
              <LandingPage />
            </Route>
          </Switch>
      </Router>
    );
  }
  