const needle = require('needle');
const token = "AAAAAAAAAAAAAAAAAAAAALW9OwEAAAAAAq%2Bht8V%2BdklsQiL%2BVt2YbLnL5Y8%3DEUo18Fgiaro7o1FxrcUZTHV7K4NFgTXRrPezVv48JXGvNvnIrC";

const endpointURL = "https://api.twitter.com/2/users/by?usernames="


exports.tweets = async(req, res) => {
    const response = await getRequest(req);
    const response2 = await getTotalResponse(response)
    res.json({
        "tweets": response2,
        "userProfile": response
    })
}

async function getRequest(req) {

    const params = {
        usernames: req.body.username, // Edit usernames to look up
        "user.fields": "created_at,description,profile_image_url,verified,withheld,public_metrics", // Edit optional query parameters here
        "expansions": "pinned_tweet_id",
        "tweet.fields": "created_at"
    }

    const res = await needle('get', endpointURL, params, {
        headers: {
            "User-Agent": "v2UserLookupJS",
            "authorization": `Bearer ${token}`
        }
    })

    if (res.body) {
        return res.body
    } else {
        throw new Error('Unsuccessful request')
    }
}

async function getTotalResponse(response) {
    console.log(response.data)
    console.log(response.data.pinned_tweet_id)

    const params = {
        "expansions":"attachments.media_keys,entities.mentions.username",
       "media.fields": "preview_image_url,url,media_key",
       "tweet.fields": "attachments,author_id,text,referenced_tweets,source"
    }

    const endpointURL = `https://api.twitter.com/2/users/${response.data[0].id}/tweets`
    const res = await needle('get', endpointURL, params, {
        headers: {
                "User-Agent": "v2UserLookupJS",
                "authorization": `Bearer ${token}`
        }
    })
    
    if (res.body) {
            return res.body
    } else {
            throw new Error('Unsuccessful request')
    }
}
