const express = require('express'),
router = express.Router(),
tweets = require('../controllers/tweets')
 
router.post('/', tweets.tweets)

module.exports = router